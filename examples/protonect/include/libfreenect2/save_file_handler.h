#include "H5Cpp.h"
#include <iostream>
#include <string>
#include <vector>

#ifndef SAVE_FILE_HANDLER_H
#define SAVE_FILE_HANDLER_H

class SaveFileHandler {

public:
  SaveFileHandler(const std::string filename);
  ~SaveFileHandler();

  void SaveBuffer(const unsigned char* arr, int length, unsigned int source);
  void SaveColorBuffer(const unsigned char* arr, int length);
  void SaveIrBuffer(const unsigned char* arr, int length);
  void SaveP0Tables(const unsigned char* arr, int length);

  void IncrementCount();

private:
  void SaveCharArray(const unsigned char* arr, int length, std::string dataset);
  hid_t GetGroup(int frame_count);

  std::string filename;
  hid_t file_id;
  std::vector<hid_t> group_id_vec;
  int frame_count;
};


#endif

