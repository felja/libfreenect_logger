//#include <pthread.h>
#include <thread>  
#include <mutex>  
#include <condition_variable>
#include <queue>  
#ifndef FRAME_LOGGER_H_
#define FRAME_LOGGER_H_

#define MAGIC_TIME_STAMP_THRESHOLD 120 //Frames that are close to eachother have a time stamp difference well within 120 units
#define MAX_FRAME_OFFSET 5 //If there is more than 5 frames difference between the frame queues we will stop searching matches and inform the user

struct FrameLoggerObject {
	FrameLoggerObject() {
		buffer_ = NULL;
	}
	FrameLoggerObject(unsigned int length, unsigned int time_stamp) {
		length_ = length;
		time_stamp_ = time_stamp;
		buffer_ = new unsigned char[length];
	}
	~FrameLoggerObject()
	{
		if(buffer_!=NULL) delete[] buffer_;
	}
	unsigned char* buffer_;
	unsigned int length_, time_stamp_;
};

class FrameLoggerThread {
public:
	FrameLoggerThread();
	~FrameLoggerThread();
	std::thread  spawn();
	void queueHandlingThread();
	bool isFramePair(unsigned int time_stamp_first, unsigned int time_stamp_second);
	void run();
	void stop();

	std::queue<FrameLoggerObject*> ir_logger_queue_;
	std::queue<FrameLoggerObject*> rgb_logger_queue_;
	std::mutex ir_mutex_, rgb_mutex_;
	std::condition_variable ir_lock_cv_, rgb_lock_cv_;

	unsigned char* p0_table_;
	unsigned int p0_table_length_;
private:
	std::thread thread_;
	bool stop_;
};

class FrameLogger {

public:
	FrameLogger();
	~FrameLogger();
	void startRecording();
	void onNewColorFrame(unsigned char* buffer, unsigned int length, unsigned int time_stamp);
	void onNewIrFrame(unsigned char* buffer, unsigned int length, unsigned int time_stamp);
	void setP0Table(unsigned char* buffer, unsigned int length);
private:
	std::mutex mutex_;
	FrameLoggerThread* logger_thread_;
	bool recording_;
};

#endif
