#include <libfreenect2/save_file_handler.h>
#include "hdf5.h"
#include "hdf5_hl.h"
#include <string>
#include <sstream>

using namespace H5;

static std::string Int2String(int val)
{
  std::string res;          // string which will contain the result
  std::ostringstream convert;   // stream used for the conversion
  convert << val;      // insert the textual representation of 'Number' in the characters in the stream
  res = convert.str();
  return res;
}

SaveFileHandler::SaveFileHandler(const std::string in_filename)
{
  //Create new file, delete existing file with the same filename
  filename = in_filename;
  file_id = H5Fcreate (filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
 
  frame_count = 0;
}

SaveFileHandler::~SaveFileHandler()
{
  /*for(unsigned int i = 0; i < group_id_vec.size(); i++)
     H5Tclose(group_id_vec[i]);*/

  group_id_vec.clear();
  //herr_t status = H5Tclose(file_id);
  H5Fclose(file_id);
}

void SaveFileHandler::SaveBuffer(const unsigned char* arr, int length, unsigned int source)
{
  switch(source)
  {
    case 1:
      SaveIrBuffer(arr, length); 
      break;
    case 2:  
      SaveColorBuffer(arr, length);
      break;
    case 4:
      SaveP0Tables(arr, length);
      break;
    default:
      std::cout<<"[SaveFileHandler] no such source "<<source<<std::endl;
  }
}

void SaveFileHandler::SaveP0Tables(const unsigned char* arr, int length)
{
  //hid_t       dataset_id, dataspace_id;
  hsize_t     dims[1];
  herr_t      status;

  dims[0] = length;
  /* create and write an char type dataset named "P0Tables" */
  std::cout<<"SaveP0Tables! \n";
  status = H5LTmake_dataset(file_id, "/P0Tables", 1, dims, H5T_NATIVE_CHAR, arr);
  /*dataspace_id = H5Screate_simple (1, dims, NULL);
  dataset_id = H5Dcreate(file_id, "/dset", H5T_STD_I32BE, dataspace_id,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);*/
}

void SaveFileHandler::SaveColorBuffer(const unsigned char* arr, int length)
{

  std::string dataset = "Color";
  SaveCharArray(arr, length, dataset);
}

void SaveFileHandler::SaveIrBuffer(const unsigned char* arr, int length)
{

  std::string dataset = "Ir";
  SaveCharArray(arr, length, dataset);
}

/* Saves a char array to file */
void SaveFileHandler::SaveCharArray(const unsigned char* arr, int length, std::string dataset)
{
  //hid_t       dataset_id, dataspace_id;

  hid_t grp_id = GetGroup(frame_count);  

  //frame_group.append(dataset);  
  herr_t      status;
  hsize_t dims[1];
  dims[0] = length;
	

  status = H5LTmake_dataset(grp_id, dataset.c_str(), 1, dims, H5T_NATIVE_CHAR, arr);

  //dataspace_id = H5Screate_simple(1, dims, NULL);

   /* Create dataset in group */
  //dataset_id = H5Dcreate2(grp_id, dataset.c_str(), H5T_STD_I32BE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  /* Write data to dataset. */
  //status = H5Dwrite(dataset_id, H5T_NATIVE_CHAR, H5S_ALL, H5S_ALL, H5P_DEFAULT, arr);
  
  /* Close the data space for the second dataset. */
  //status = H5Sclose(dataspace_id);

  /* Close the second dataset */
  //status = H5Dclose(dataset_id);
  //status = H5LTmake_dataset(grp_id, dataset.c_str() , 1 , dims, H5T_NATIVE_CHAR, arr);  

}

hid_t SaveFileHandler::GetGroup(int frame_count)
{
  if(frame_count > group_id_vec.size())
  {
     std::string frame_count_str = Int2String(frame_count-1);
     std::string frame_group = "/Frame_";
     frame_group.append(frame_count_str);

     hid_t grp_id = H5Gcreate2(file_id, frame_group.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
     group_id_vec.push_back(grp_id);  

     return grp_id;
  }
  else
      return group_id_vec.back();
}

void SaveFileHandler::IncrementCount()
{
   frame_count++;
}

