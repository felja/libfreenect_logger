#include <stdio.h>
#include <cstring>
#include <ctime>
#include <sstream>
#include <chrono>  
#include <libfreenect2/frame_logger.h>
#include <libfreenect2/save_file_handler.h>

FrameLogger::FrameLogger()
{
	logger_thread_ = new FrameLoggerThread();
	recording_ = false;
}

FrameLogger::~FrameLogger()
{
	delete logger_thread_;
}

void FrameLogger::startRecording()
{
	recording_ = true;
	logger_thread_->run();
}

void FrameLogger::onNewColorFrame(unsigned char* buffer, unsigned int length, unsigned int time_stamp)
{
	//std::cout<<"color time_stamp = "<<time_stamp<<std::endl;
	if(!recording_)
		return;

	FrameLoggerObject* frame = new FrameLoggerObject(length,time_stamp);
	std::memcpy (frame->buffer_, buffer, length);
	
	std::unique_lock<std::mutex> lck(logger_thread_->rgb_mutex_);
	logger_thread_->rgb_logger_queue_.push(frame);
	//tell waiting thread new rgb frame has arrived 
	logger_thread_->rgb_lock_cv_.notify_one();

	
}

void FrameLogger::onNewIrFrame(unsigned char* buffer, unsigned int length, unsigned int time_stamp)
{
	//std::cout<<"ir time_stamp = "<<time_stamp<<std::endl;
	if(!recording_)
		return;

	FrameLoggerObject* frame = new FrameLoggerObject(length,time_stamp);
	std::memcpy (frame->buffer_, buffer, length);
	
	std::unique_lock<std::mutex> lck(logger_thread_->ir_mutex_);
	logger_thread_->ir_logger_queue_.push(frame);
	//tell waiting thread new ir frame has arrived
	logger_thread_->ir_lock_cv_.notify_one();

	
}

void FrameLogger::setP0Table(unsigned char* buffer, unsigned int length)
{
	logger_thread_->p0_table_ = new unsigned char[length];
	memcpy (logger_thread_->p0_table_, buffer, length);
	logger_thread_->p0_table_length_ = length;
}

FrameLoggerThread::FrameLoggerThread()
{
	stop_ = true;
}

FrameLoggerThread::~FrameLoggerThread()
{
	if(stop_ == false)
		stop();

	delete[] p0_table_;
}

void FrameLoggerThread::run()
{
	//init thread
	stop_ = false;
	thread_ = spawn();
}

void FrameLoggerThread::stop()
{
	stop_ = true;
	ir_lock_cv_.notify_one();
	rgb_lock_cv_.notify_one();
	thread_.join();

	while(!ir_logger_queue_.empty())
	{
		delete ir_logger_queue_.front();
		ir_logger_queue_.pop();	
	}

	while(!rgb_logger_queue_.empty())
	{
		delete rgb_logger_queue_.front();
		rgb_logger_queue_.pop();	
	}
}

std::thread FrameLoggerThread::spawn() 
{
	return std::thread( [this] { this->queueHandlingThread(); } );
}

void FrameLoggerThread::queueHandlingThread()
{
	//create h5 file with unique name derived from current time
	std::time_t result = std::time(nullptr);
	std::string res;          
  std::ostringstream convert; 
  convert <<  result;    
  res = convert.str();
	res.append("video_sequence.h5");
	SaveFileHandler file(res);
	file.SaveP0Tables(p0_table_, p0_table_length_);

	int count, no_frame_pair_found_count;
	std::vector<FrameLoggerObject*> tmp_rgb_logger_queue_;
	no_frame_pair_found_count = 0;
	while(!stop_)
	{
		//Read first ir frame

		std::unique_lock<std::mutex> ir_lck(ir_mutex_);
		
		//wait if queue is empty
		while(ir_logger_queue_.empty() && !stop_)
			ir_lock_cv_.wait(ir_lck);

		if(stop_)
			return;

		FrameLoggerObject* ir_frame = ir_logger_queue_.front();
		ir_logger_queue_.pop();

		ir_lck.unlock();

		//Find corresponding rgb frame within some range		
		bool frame_pair_found = false;
		count = 0;
		while(!frame_pair_found && count < MAX_FRAME_OFFSET)
		{
			FrameLoggerObject* rgb_frame;
			if(tmp_rgb_logger_queue_.size() <= count)
			{
				//rgb_mutex_.lock();
				std::unique_lock<std::mutex> rgb_lck(rgb_mutex_);

				//wait if queue is empty
				while(rgb_logger_queue_.empty() && !stop_)
					rgb_lock_cv_.wait(rgb_lck);
				
				if(stop_)
					return;


				rgb_frame = rgb_logger_queue_.front();
				rgb_logger_queue_.pop();
				rgb_lck.unlock();

				tmp_rgb_logger_queue_.push_back(rgb_frame);
			}
			else
			{
				rgb_frame = tmp_rgb_logger_queue_[count];
			}
		

			if(isFramePair(rgb_frame->time_stamp_,ir_frame->time_stamp_))
			{
				//frame pair found
				frame_pair_found = true;

				file.IncrementCount();
				//save to disk
				file.SaveIrBuffer(ir_frame->buffer_, ir_frame->length_);
				file.SaveColorBuffer(rgb_frame->buffer_, rgb_frame->length_);
				
			}

			count++;
		}

		delete ir_frame;

		if(frame_pair_found)
		{
			//free memory
			for(int i = 0; i < count; i++)
			{
				delete tmp_rgb_logger_queue_[0];
				tmp_rgb_logger_queue_.erase(tmp_rgb_logger_queue_.begin());
			}
			no_frame_pair_found_count = 0;
		}
		else
			no_frame_pair_found_count++;
		
		if(no_frame_pair_found_count>MAX_FRAME_OFFSET)
		{
			//no frame pairs have been matched for a while
			while(!stop_)
			{	
				std::cout<<"ALERTION: Frame streams are out of sync"<<std::endl;
				std::this_thread::sleep_for (std::chrono::seconds(1));
			}
			return;
		}
	}	
	
	return;
}

bool FrameLoggerThread::isFramePair(unsigned int time_stamp_first, unsigned int time_stamp_second)
{
	return (abs(time_stamp_first-time_stamp_second) < MAGIC_TIME_STAMP_THRESHOLD) ||
				 (abs(0xFFFF-time_stamp_first-time_stamp_second) < MAGIC_TIME_STAMP_THRESHOLD) ||
				 (abs(time_stamp_first-(0xFFFF-time_stamp_second))  < MAGIC_TIME_STAMP_THRESHOLD);
}


